from object2dictionary import (
    to_dictionaries,
    to_camel_case,
    to_snake_case,
    to_nested_camel_case,
    to_nested_snake_case, )


# ------ Name Formatting Tests ------ #

def test_name_formatting():
    test_cases = {
        'name': 'name',
        'variable_name': 'variableName',
        'variable_name_two': 'variableNameTwo'}
    for key, value in test_cases.items():
        assert to_camel_case(key) == value
        assert to_snake_case(value) == key
    print('single name formatting test complete')

def test_nested_name_formatting():
    alpha = {'variable_name': 'alpha', 'class_': 0, 'excluded_name': 0}
    bravo = {'variable_name': 'bravo', 'nested': [alpha], 'region_USA': 0}
    output = to_nested_camel_case(
        bravo, 
        exclusions = ['excluded_name'],
        custom_rules = {'region_USA':'regionUSA'}, )
    assert output['variableName'] == 'bravo'
    assert output['nested'][0]['variableName'] == 'alpha'
    assert output['nested'][0]['class_'] == 0
    assert 'excluded_name' in output['nested'][0].keys()
    assert 'regionUSA' in output.keys()
    print('nested name formatting test complete')


# ------ Object to Dictionary Conversion Tests ------ #

class MockOne:
    
    def __init__(self):
        self.attribute = 'Hello World 1'
        self._internal_attribute = 'Hello World 2'
        self.nested_item = [MockTwo()]
        self.nested_item[0].attribute = 'Hello World 3'
        self.dangerous_circular_reference = self
    
    @property
    def this_is_a_property(self):
        return self._internal_attribute
    
    @property
    def another_property(self):
        return 12345
    
    def on_before_serialize(self, dictionary):
        del dictionary['dangerous_circular_reference']
    
    properties_not_to_serialize = 'another_property'

class MockTwo: pass 

def test_nested_object_to_dictionary_conversion():
    mock = MockOne()
    output = to_dictionaries([mock])[0]
    assert type(output) == dict
    assert output['attribute'] == 'Hello World 1'
    assert '_internal_attribute' not in output.keys()
    assert output['this_is_a_property'] == 'Hello World 2'
    assert (
        'another_property' not in output.keys()
        and 12345 not in output.values() )
    assert output['nested_item'][0]['attribute'] == 'Hello World 3'
    assert 'dangerous_circular_reference' not in output.keys()
    print('nested object to dictionary conversion test complete')


if __name__ == '__main__':
    test_name_formatting()
    test_nested_name_formatting()
    test_nested_object_to_dictionary_conversion()
