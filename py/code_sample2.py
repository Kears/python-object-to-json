from object2dictionary import to_nested_camel_case


sample_dictionary = {
    
    # This key will be converted to 'keyName'.
    'key_name': 1,
    
    # On line 27 I will exclude this key from camel case conversion
    'excluded_key': 3,
    
    # On line 30 I will use a custom rule to convert this key to "regionUSA"
    # instead of the default output of "regionUsa"
    'region_USA': 4
    
}

# Now let's arbitrarily nest the dictionary.
nested_items = [1, 2, {'key': sample_dictionary}]


# Convert snake case keys to camel case.
output = to_nested_camel_case(
    
    nested_items,
    
    # Line 27: Inform the function which keys not to convert to camel case.
    exclusions = ['excluded_key'],
    
    # Line 30: Inform the function which key to replace with a custom
    # replacement key. 
    custom_rules = {'region_USA': 'regionUSA'}
    
)

# Verify the results.
item = output[2]['key']
assert item['keyName'] == 1
assert item['excluded_key'] == 3
assert item['regionUSA'] == 4


print('executable code sample complete')
