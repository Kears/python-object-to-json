""" Recursively converts objects to dictionaries for JSON encoding.

This library provides a tool to recursively convert Python objects into
dictionaries for JSON encoding. Python's built-in JSON encoder can encode
dictionaries but not objects. This library also provides a tool to
recursively convert snake case variables to camel case variables, for
example, from `hello_world` to `helloWorld`.

To illustrate a use case for this library, the Python backend of a web
application can use these tools to encode Python objects to JSON to send them
to a JavaScript frontend. The camel case converter ensures that the variable
names follow JavaScript's naming conventions.

Both of these tools operate recursively on a tree of arbitrarily nested Python
objects and data structures. When the object to dictionary converter
encounters a nested object, it converts it into a dictionary. Afterwards, the
converter assigns the dictionary to the place previously occupied by the
object. The object to dictionary converter carries out this operation on
every nested object. This automates what would otherwise be a manual step for
each nested object.

Public Functions:
    to_dictionaries
    to_camel_case
    to_snake_case
    to_nested_camel_case
    to_nested_snake_case

"""


# ------ Converting Nested Objects to Dictionaries ------ #

def to_dictionaries(item):
    """ Recursively converts objects to dictionaries in a tree of nested 
    objects and other items.

    Arguments:
        item: An object, data structure or tree of nested objects and data
            structures (for example, a tuple containing a set containing 
            integers and an object)

    Returns:
        The same tree of nested items, but with each object replaced by a
        dictionary. The dictionary created from an object gets assigned to the 
        same place previously occupied by the object. Otherwise, the location 
        and structure of every item remains the same.

    Excluded Attributes:

    This function ignores object attributes whose names start with a leading
    underscore. Those attributes and their values do not get converted into
    dictionary entries.

    Python's official style guide, PEP8, contains a section explaining the
    role of leading and trailing underscores in Python variable names. A
    single leading underscore in a variable name indicates an "internal use"
    variable. Python applies name mangling to class variable names with two
    leading underscores. Name mangling protects against accidental
    manipulation of such variables in subclasses. Finally, Python uses two
    leading and two trailing underscores in a variable name to designate
    a "magic" object or attribute. These serve a special purpose in the
    Python language.

    This function presumes that all of the variables described in the
    paragraph above do not belong in your JSON data. Ignoring object
    attributes whose names start with one or two leading underscores ensures
    that these variables stay out of your JSON.

    Python Properties:

    A Python property is a special type of object variable. Accessing a
    property calls its getter method and gives you the value returned by that
    method. Setting the value of a property calls its setter method to do
    so. 

    If an object has a Python property, this function converts the property
    into an entry in the dictionary created from that object. A dictionary
    entry consists of a key-value pair. This function sets the key to the
    property name and sets the value to the value returned by calling the
    getter method.

    You can instruct this function not to convert specific properties into
    dictionary entries. To do so, create a class attribute in the object's
    class called "properties_not_to_serialize". Assign to that attribute a
    list or tuple of strings where each string is the name of a property this
    function should ignore. Each class can have its
    own "properties_not_to_serialize" class attribute to declare excluded
    properties for objects created from that class.

    Other Data Structures:

    This function also converts sets and frozen sets into lists so they can be
    encoded as JSON arrays. Python's built-in JSON encoder cannot encode sets
    or frozen sets by default. 

    Post-Processing Hooks:

    A programming hook is a way to optionally customize the behavior of the
    programming tool associated with it. This function supports optional
    hooks to post-process the dictionaries it creates from objects. Each
    object class can have a its own post-processing hook.

    To use this hook with an object, the object's class should implement a
    method called "on_before_serialize". This function will pass the
    dictionary created from that object to the method. At that point, any
    objects among the dictionary values have not been converted to objects
    yet. The method can then make in-place changes to the dictionary. Each
    object class can implement such a method to have its own post-processing
    hook.

    To illustrate one use case for this post-processing hook, consider an
    object that contains a circular object reference. Python's JSON encoder
    will raise an error when it encounters this to avoid an infinite regress.
    You can use the post-processing hook to delete or replace the circular
    reference.
    """
    if _is_writable_object(item):
        item = _single_object_to_dictionary(item)
    type_ = type(item)
    if type_ is dict:
        for key, member in item.copy().items():
            item[key] = to_dictionaries(member)
    elif type_ in (list, tuple, set, frozenset):
        if type_ in (tuple, set):
            item = list(item)
        for index in range(len(item)):
            item[index] = to_dictionaries(item[index])
    return item

def _is_writable_object(item):
    return hasattr(item, '__dict__')

def _single_object_to_dictionary(item):
    dictionary = vars(item).copy()
    dictionary.update(_get_properties_to_serialize(item))
    _remove_leading_underscore_attributes(dictionary)
    if hasattr(item, 'on_before_serialize'):
        returned = item.on_before_serialize(dictionary)
        if type(returned) is dict:
            dictionary = returned
    return dictionary

def _get_properties_to_serialize(item):
    for property_name, property_value in _get_all_properties(item):
        is_ignored = (
            hasattr(item, 'properties_not_to_serialize')
            and property_name in item.properties_not_to_serialize )
        if not is_ignored:
            yield property_name, property_value

def _get_all_properties(obj):
    class_attributes = vars(type(obj))
    for key, value in class_attributes.items():
        if type(value) == property:
            yield key, getattr(obj, key)

def _remove_leading_underscore_attributes(dict):
    for key in dict.copy().keys():
        if key[0] == '_':
            del dict[key]


# ------ Name Formatting ------ #

def to_camel_case(snake_case):
    """ Converts a single string from snake case to camel case, e.g. 
    "hello_world" to "helloWorld". """
    words = snake_case.split('_')
    new_words = [words[0]] + [word.title() for word in words[1:]]
    return ''.join(new_words)

def to_snake_case(camel_case):
    """ Converts a single string from camel case to snake case, e.g. 
    "helloWorld" to "hello_world". """
    new_characters = []
    for character in list(camel_case):
        if character.isupper():
            new_characters.append('_')
            new_characters.append(character.lower())
        else:
            new_characters.append(character)
    return ''.join(new_characters)

def to_nested_camel_case(dicts, exclusions = [], custom_rules = {}):
    """ Recursively converts dictionary keys from snake case to camel case in
    a tree of nested dictionaries and/or lists.
    
    Arguments:
        dicts: A tree of nested dictionaries and/or lists.
        exclusions: An optional list or tuple of strings where each string is 
            the name of a dictionary key to skip.
        custom_rules: An optional dictionary of custom conversion rules where
            each key-value pair corresponds to a name and a custom replacement
            name respectively.
    
    Returns:
        The same tree of nested dictionaries and/or lists with its dictionary
        keys renamed accordingly.
    """
    return _format_nested_names(dicts, to_camel_case, exclusions, custom_rules)

def to_nested_snake_case(dicts, exclusions = [], custom_rules = {}):
    """ Recursively converts dictionary keys from camel case to snake case in a
    tree of nested dictionaries and/or lists.
    
    Arguments:
        dicts: A tree of nested dictionaries and/or lists.
        exclusions: An optional list or tuple of strings where each string is
            the name of a dictionary key to skip.
        custom_rules: An optional dictionary of custom conversion rules where
            each key-value pair corresponds to a name and a custom replacement
            name respectively.
    
    Returns:
        The same tree of nested dictionaries and/or lists with its dictionary 
        keys renamed accordingly.
    """
    return _format_nested_names(dicts, to_snake_case, exclusions, custom_rules)

def _format_nested_names(dicts, format_function, exclusions, custom_rules):
    for dictionary in _get_nested_dictionaries(dicts):
        for key, value in dictionary.copy().items():
            if key in exclusions or _has_trailing_underscore(key):
                continue
            del dictionary[key]
            if key in custom_rules.keys():
                replacement_key = custom_rules[key]
                dictionary[replacement_key] = value
            else:
                replacement_key = format_function(key)
                dictionary[replacement_key] = value
    return dicts

def _get_nested_dictionaries(item):
    type_ = type(item)
    if type_ not in (list, dict):
        return
    if type_ is dict:
        yield item
    members = item.values() if type_ is dict else item
    for member in members:
        yield from _get_nested_dictionaries(member)

def _has_trailing_underscore(string):
    return string[-1] == '_'
