from object2dictionary import to_dictionaries


# Define the class to create the sample object from.
class SampleObjectClass:
    
    def __init__(self):
        
        # This attribute will be converted into a dictionary entry, e.g.
        # {'attribute': 'Hello World 1'}.
        self.attribute = 'Hello World 1'
        
        # An attribute with a leading underscore such as this will NOT be
        # converted into a dictionary entry.
        self._backing_field = 'Hello World 2'
    
    # This property getter will be used to create a dictionary entry, e.g.
    # {'property1': 'Hello World 2'}.
    @property
    def property1(self):
        return self._backing_field
    
    # The "properties_not_to_serialize" class attribute below indicates that
    # this property should not become a dictionary entry.
    @property
    def property2(self):
        return 12345
    
    properties_not_to_serialize = ['property2']


# Now let's arbitrarily nest an object instance in a tree of objects and data
# structures.
sample_object = SampleObjectClass()
class SomeOtherObjectClass: pass
other_object = SomeOtherObjectClass()
other_object.other_attribute = sample_object
nested_items = 1, 2, {'key': [3, 4, other_object]}


# Convert objects in the nested items to dictionaries.
output = to_dictionaries(nested_items)

# Serialize the result into JSON.
import json
json_output = json.dumps(output, separators = (', ', ': '))

# Verify the JSON.
expected_json = '[1, 2, {"key": [3, 4, {"other_attribute": {"attribute": "Hello World 1", "property1": "Hello World 2"}}]}]'
assert json_output == expected_json


print('executable code sample complete')
