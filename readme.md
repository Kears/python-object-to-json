# Python Object To JSON

This library addresses the problem raised in this [Stack Overflow][] question.
Specifically, this library provides a tool to recursively convert Python
objects into dictionaries for JSON encoding. Python's built-in JSON encoder
can encode dictionaries but not objects. This library also provides a tool to
recursively convert snake case variables to camel case variables, for example,
from `hello_world` to `helloWorld`.

To illustrate a use case for this library, the Python backend of a web
application can use these tools to encode Python objects to JSON to send them
to a JavaScript frontend. The camel case converter ensures that the variable
names follow JavaScript's naming conventions.

Both of these tools operate recursively on a tree of arbitrarily nested Python
objects and data structures. When the object to dictionary converter
encounters a nested object, it converts the object into a dictionary.
Afterwards, the converter assigns the dictionary to the place previously
occupied by the object. The object to dictionary converter carries out this
operation on every nested object. This automates what would otherwise be a
manual step for each nested object.


## Requirements

This library has no dependencies other than Python 3. I developed and tested
this library using Python 3.9 but I haven't tested its compatibility with
other versions of Python 3.


## Installation

This library is not hosted on PyPI and cannot be installed using `pip`. To use
the library, clone the repository, copy the `object2dictionary.py` file
to your project directory and import it as a module.


## Tutorial & Code Samples

### Convert Nested Dictionaries to Objects for JSON Encoding

```python
from object2dictionary import to_dictionaries


# Define the class to create the sample object from.
class SampleObjectClass:
    
    def __init__(self):
        
        # This attribute will be converted into a dictionary entry, e.g.
        # {'attribute': 'Hello World 1'}.
        self.attribute = 'Hello World 1'
        
        # An attribute with a leading underscore such as this will NOT be
        # converted into a dictionary entry.
        self._backing_field = 'Hello World 2'
    
    # This property getter will be used to create a dictionary entry, e.g.
    # {'property1': 'Hello World 2'}.
    @property
    def property1(self):
        return self._backing_field
    
    # The "properties_not_to_serialize" class attribute below indicates that
    # this property should not become a dictionary entry.
    @property
    def property2(self):
        return 12345
    
    properties_not_to_serialize = ['property2']


# Now let's arbitrarily nest an object instance in a tree of objects and data
# structures.
sample_object = SampleObjectClass()
class SomeOtherObjectClass: pass
other_object = SomeOtherObjectClass()
other_object.other_attribute = sample_object
nested_items = 1, 2, {'key': [3, 4, other_object]}


# Convert objects in the nested items to dictionaries.
output = to_dictionaries(nested_items)

# Serialize the result into JSON.
import json
json_output = json.dumps(output, separators = (', ', ': '))

# Verify the JSON.
expected_json = '[1, 2, {"key": [3, 4, {"other_attribute": {"attribute": "Hello World 1", "property1": "Hello World 2"}}]}]'
assert json_output == expected_json
```

### Convert Dictionary Keys to Camel Case

```python
from object2dictionary import to_nested_camel_case


sample_dictionary = {
    
    # This key will be converted to 'keyName'.
    'key_name': 'value1',
    
    # On line 27 I will exclude this key from camel case conversion
    'excluded_key': 'value2',
    
    # On line 30 I will use a custom rule to convert this key to "regionUSA"
    # instead of the default output of "regionUsa"
    'region_USA': 'value3'
    
}

# Now let's arbitrarily nest the dictionary.
nested_items = [1, 2, {'key': sample_dictionary}]


# Convert snake case keys to camel case.
output = to_nested_camel_case(
    
    nested_items,
    
    # Line 27: Inform the function which keys not to convert to camel case.
    exclusions = ['excluded_key'],
    
    # Line 30: Inform the function which key to replace with a custom
    # replacement key. 
    custom_rules = {'region_USA': 'regionUSA'}
    
)

# Verify the results.
item = output[2]['key']
assert item['keyName'] == 'value1'
assert item['excluded_key'] == 'value2'
assert item['regionUSA'] == 'value3'
```


## API Reference

### to_dictionaries(item)

Recursively converts objects to dictionaries in a tree of nested objects and
other items.

*Arguments*

- `item` An object, data structure or tree of nested objects and data
  structures (for example, a tuple containing a set containing integers and
  an object)

*Returns*

- The same tree of nested items, but with each object replaced by a
  dictionary. The dictionary created from an object gets assigned to the same
  place previously occupied by the object. Otherwise, the location and
  structure of every item remains the same.

*Excluded Attributes*

This function ignores object attributes whose names start with a leading
underscore. Those attributes and their values do not get converted into
dictionary entries.

Python's official style guide, [PEP8][], contains a
[section][naming conventions] explaining the role of leading and trailing 
underscores in Python variable names. A single leading underscore in a
variable name indicates an "internal use" variable. Python applies
[name mangling][] to class variable names with two leading underscores. Name
mangling protects against accidental manipulation of such variables in
subclasses. Finally, Python uses two leading and two trailing underscores in
a variable name to designate a "magic" object or attribute. These serve a
special purpose in the Python language.

This function presumes that all of the variables described in the paragraph
above do not belong in your JSON data. Ignoring object attributes whose names
start with one or two leading underscores ensures that these variables stay
out of your JSON.

*Python Properties*

A Python property is a special type of object variable. Accessing a property
calls its getter method and gives you the value returned by that method. 
Setting the value of a property calls its setter method to do so. 

If an object has a Python property, this function converts the property into
an entry in the dictionary created from that object. A dictionary entry
consists of a key-value pair. This function sets the key to the property name
and sets the value to the value returned by calling the getter method.

You can instruct this function not to convert specific properties into
dictionary entries. To do so, create a class attribute in the object's class
called "properties_not_to_serialize". Assign to that attribute a list or
tuple of strings where each string is the name of a property this function
should ignore. Each class can have its own "properties_not_to_serialize"
class attribute to declare excluded properties for objects created from that
class.

*Other Data Structures*

This function also converts sets and frozen sets into lists so they can be
encoded as JSON arrays. Python's built-in JSON encoder cannot encode sets or
frozen sets by default. 

*Post-Processing Hooks*

A programming hook is a way to optionally customize the behavior of the
programming tool associated with it. This function supports optional hooks to
post-process the dictionaries it creates from objects. Each object class can
have a its own post-processing hook.

To use this hook with an object, the object's class should implement a method
called "on_before_serialize". This function will pass the dictionary created
from that object to the method. At that point, any objects among the
dictionary values have not been converted to objects yet. The method can then
make in-place changes to the dictionary. Each object class can implement such
a method to have its own post-processing hook.

To illustrate one use case for this post-processing hook, consider an object
that contains a circular object reference. Python's JSON encoder will raise
an error when it encounters this to avoid an infinite regress. You can use the
post-processing hook to delete or replace the circular reference.

### to_camel_case(snake_case)

Converts a single string from snake case to camel case, e.g. "hello_world" to
"helloWorld".

### to_snake_case(camel_case)

Converts a single string from camel case to snake case, e.g. "helloWorld" to
"hello_world".

### to_nested_camel_case(dicts, exclusions = [], custom_rules = {})

Recursively converts dictionary keys from snake case to camel case in a
tree of nested dictionaries and/or lists.

*Arguments*

- `dicts` A tree of nested dictionaries and/or lists.
- `exclusions` An optional list or tuple of strings where each string is the
  name of a dictionary key to skip.
- `custom_rules` An optional dictionary of custom conversion rules where each
  key-value pair corresponds to a name and a custom replacement name
  respectively.

*Returns*

- The same tree of nested dictionaries and/or lists with its dictionary keys
  renamed accordingly.

### to_nested_snake_case(dicts, exclusions = [], custom_rules = {})

Recursively converts dictionary keys from camel case to snake case in a
tree of nested dictionaries and/or lists.

*Arguments*

- `dicts` A tree of nested dictionaries and/or lists.
- `exclusions` An optional list or tuple of strings where each string is the
  name of a dictionary key to skip.
- `custom_rules` An optional dictionary of custom conversion rules where each
  key-value pair corresponds to a name and a custom replacement name
  respectively.

*Returns*

- The same tree of nested dictionaries and/or lists with its dictionary keys
  renamed accordingly.


[Stack Overflow]:https://stackoverflow.com/questions/3768895/how-to-make-a-class-json-serializable
[PEP8]:https://www.python.org/dev/peps/pep-0008/#naming-conventions
[naming conventions]:https://peps.python.org/pep-0008/#descriptive-naming-styles
[name mangling]:https://docs.python.org/3/tutorial/classes.html#private-variables
